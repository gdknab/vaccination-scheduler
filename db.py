# -*- coding: utf-8 -*-
from pymongo import MongoClient
from config import *
from datetime import datetime
from bson.objectid import ObjectId


def get_cli():
  client = MongoClient('localhost', MONGO_PORT)
  return client.parker


def admin_email_exists(email):
  col = get_cli().admin_users
  return col.find({
    'email': email,
  }).count() > 0

def check_admin_user(email, pw):
  col = get_cli().admin_users
  return col.find({
    'email': email,
    'password': pw
  }).count() > 0


def admin_update_login_date(email):
  col = get_cli().admin_users
  return col.update({
    'email': email,
  }, {
    '$set': {
      'last_login': datetime.now()
    }
  })


def get_admin_user(email):
  col = get_cli().admin_users
  return col.find_one({
    'email': email
  })

def get_admin_users():
  col = get_cli().admin_users
  return col.find({})

def admin_add_user(email, pw, creator, name, level=0):
  col = get_cli().admin_users
  return col.insert_one({
    'creator': creator,
    'email': email,
    'name': name,
    'password': pw,
    'level': level,
    'ts': datetime.now(),
    'must_change_pw': True
  })

def admin_change_pw_insec(email, npw):
  col = get_cli().admin_users
  return col.update({
    'email': email,
  }, {
    '$set': {
      'password': npw,
      'must_change_pw': True
    }
  })

def admin_change_pw(email, cpw, npw):
  col = get_cli().admin_users
  return col.update({
    'email': email,
    'password': cpw
  }, {
    '$set': {
      'password': npw,
      'must_change_pw': False
    }
  })

def delete_admin_user(email):
  col = get_cli().admin_users
  return col.remove({
    'email': email
  })











def add_person(name, phone, phone_national, walk_in, author):
  col = get_cli().people
  return col.insert_one({
    'name': name,
    'phone': phone,
    'status': STATUS_TO_CONTACT,
    'phone_national': phone_national,
    'walk_in': walk_in,
    'author': author,
    'ts': datetime.now()
  })

def set_elig_and_dob(name, phone, elig, dob, skin_colour, is_hispanic, patient):
  col = get_cli().people
  return col.update_one({
    'name': name,
    'phone': phone,
  }, {
    '$set': {
      'status': STATUS_CONTACTED,
      'justification': elig,
      'dob': dob,
      'patient': patient,
      'skin_colour': skin_colour,
      'is_hispanic': is_hispanic,
      'waiting_since': datetime.now()
    }
  })

def get_people_with_status(status):
  col = get_cli().people
  return col.find({'status': status}, {'_id': False})

def get_people_with_status_and_query(status, q):
  col = get_cli().people
  return col.find({
    'status': status,
    '$or': [
      {'name': {'$regex': '.*%s.*' % ' '.join(map(lambda x: x.capitalize(), q.split(' ')))}},
      {'name': {'$regex': '.*%s.*' % q}},
      {'phone': {'$regex': '.*%s.*' % q}},
      {'phone': {'$regex': '.*%s.*' % q.replace(' ', '')}},
      {'phone_national': {'$regex': '.*%s.*' % q}},
    ]
  }, {'_id': False})

def get_people_with_status_and_date(status, start):
  midnight = start.replace(hour=23, minute=59, second=59)
  col = get_cli().people
  return col.find({
    'status': status,
    '$or': [
      {
        'first_dose_dt': {
          '$gt': start,
          '$lt': midnight,
        }
      }, {
        'second_dose_dt': {
          '$gt': start,
          '$lt': midnight,
        },
      }
    ], 
  }, {'_id': False})


def get_people_with_status_query_and_date(status, q, start):
  midnight = start.replace(hour=23, minute=59, second=59)
  col = get_cli().people
  return col.find({
    'status': status,
    '$or': [
      {
        'first_dose_dt': {
          '$gt': start,
          '$lt': midnight,
        }
      }, {
        'second_dose_dt': {
          '$gt': start,
          '$lt': midnight,
        },
      }
    ], 
    '$or': [
      {'name': {'$regex': '.*%s.*' % ' '.join(map(lambda x: x.capitalize(), q.split(' ')))}},
      {'name': {'$regex': '.*%s.*' % q}},
      {'phone': {'$regex': '.*%s.*' % q}},
      {'phone': {'$regex': '.*%s.*' % q.replace(' ', '')}},
      {'phone_national': {'$regex': '.*%s.*' % q}},
    ]
  }, {'_id': False})

def get_people(ascending=False):
  col = get_cli().people
  return col.find({}, {'_id': False}).sort('ts', 1 if ascending else -1)

def get_person(name, number):
  col = get_cli().people
  return col.find({'phone': number, 'name': name}, {'_id': False})

def get_patients():
  col = get_cli().people
  return col.find({
    'patient': True
  })

def rm_person(name, number):
  col = get_cli().people
  return col.delete_one({
    'phone': number,
    'name': name
  })


def change_info(name, number, new_name, new_number, new_number_national):
  col = get_cli().people
  return col.update_one({
    'name': name,
    'phone': number 
  }, {
    '$set': {
      'name': new_name,
      'phone': new_number,
      'phone_national': new_number_national,
    }
  })



def get_schedule_for_day(start):
  midnight = start.replace(hour=23, minute=59, second=59)
  col = get_cli().people
  return col.find({
    '$or': [
      {
        'first_dose_dt': {
          '$gt': start,
          '$lt': midnight,
        }
      }, {
        'second_dose_dt': {
          '$gt': start,
          '$lt': midnight,
        },
      }
    ]
  })

def set_first_dose_date(name, phone, dt, time):
  col = get_cli().people
  return col.update_one({
    'name': name,
    'phone': phone
  }, {
    '$set': {
      'status': STATUS_SCHEDULED,
      'scheduled_since': datetime.now(),
      'first_dose_dt': dt,
      'first_dose_time': time,
    }
  })


# Only used if rescheduling from the +28 days
def set_second_dose_date(name, phone, dt, time):
  col = get_cli().people
  return col.update_one({
    'name': name,
    'phone': phone
  }, {
    '$set': {
      'status': STATUS_HAS_FIRST_DOSE,
      'scheduled_since': datetime.now(),
      'second_dose_dt': dt,
      'second_dose_time': time,
    }
  })


def move_back_to_waitlist(name, phone, dose='first'):
  if dose not in ['first', 'second']:
    return -1
  col = get_cli().people
  return col.update_one({
    'name': name,
    'phone': phone
  }, {
    '$set': {
      'status': STATUS_CONTACTED,
      'waiting_since': datetime.now(),
      '%s_dose_dt' % dose: None,
      '%s_dose_time' % dose: None,
      '%s_dose_receipt_recorded' % dose: None,
      '%s_dose_author' % dose: None,
    }
  })

def set_received_first_dose(name, phone, second_dt, second_time, author, single_dose=False):
  col = get_cli().people
  return col.update_one({
    'name': name,
    'phone': phone
  }, {
    '$set': {
      'single_dose': single_dose,
      'status': STATUS_HAS_FIRST_DOSE,
      'second_dose_dt': second_dt,
      'second_dose_time': second_time,
      'first_dose_receipt_recorded': datetime.now(),
      'first_dose_author': author
    }
  })

def set_received_second_dose(name, phone, single_dose, author):
  col = get_cli().people
  return col.update_one({
    'name': name, 
    'phone': phone
  }, {
    '$set': {
      'single_dose': single_dose,
      'status': STATUS_HAS_SECOND_DOSE,
      'second_dose_receipt_recorded': datetime.now(),
      'second_dose_author': author
    }
  })

def unset_received_second_dose(name, phone):
  col = get_cli().people
  return col.update_one({
    'name': name, 
    'phone': phone
  }, {
    '$set': {
      'status': STATUS_HAS_FIRST_DOSE,
      'second_dose_receipt_recorded': None,
      'second_dose_author': None 
    }
  })

def get_received_second_dose():
  col = get_cli().people
  return col.find({
    'status': STATUS_HAS_SECOND_DOSE
  })


def enqueue_mail(recpt, subj, body, html):
  col = get_cli().mail_queue
  return col.insert_one({
    'recpt': recpt,
    'subj': subj,
    'body': body,
    'html': html,
    'ts': datetime.now()
  })

def dequeue_mail():
  col = get_cli().mail_queue
  return col.find({}).sort([
    ('ts', -1)
  ])
  
def remove_mail(id):
  col = get_cli().mail_queue
  return col.delete_one({
    '_id': ObjectId(id)
  })
