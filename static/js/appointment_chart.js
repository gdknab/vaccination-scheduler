function hideGraph() {
  document.getElementById('appointment_chart_container').style.display = 'none';
}
function createGraph(data, first_date, last_date) {
  document.getElementById('appointment_chart_container').style.display = 'block';
  function barChartPlotter(e) {
    var ctx = e.drawingContext;
    var points = e.points;
    var y_bottom = e.dygraph.toDomYCoord(0);

    var min_sep = Infinity;
    for (var i = 1; i < points.length; i++) {
      var sep = points[i].canvasx - points[i - 1].canvasx;
      if (sep < min_sep) min_sep = sep;
    }
    var bar_width = Math.floor(2.0 / 3 * min_sep);

    ctx.fillStyle ='rgb(104,183,216)';
    for (var i = 0; i < points.length; i++) {
      var p = points[i];
      var center_x = p.canvasx;


      ctx.fillRect(center_x - bar_width / 2, p.canvasy,
        bar_width, y_bottom - p.canvasy);

      ctx.strokeStyle = '#181818';
      ctx.strokeRect(center_x - bar_width / 2, p.canvasy,
        bar_width, y_bottom - p.canvasy);
    }
  }


  new Dygraph(
    document.getElementById('appointment_chart'), data, {
      legend: 'always',
      includeZero: true,
      dateWindow: [ first_date, last_date ],
      plotter: barChartPlotter,
      colors: ["#181818"],
      axes: {
        x: {
          drawGrid: false
        }
      }
    }
  );
}
