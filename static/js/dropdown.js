function toggleDropdown(id) {
  var drp = document.getElementById(id),
      tog = document.getElementById(id + '_toggle'),
      caret = document.getElementById(id + '_caret'),
      cover = document.getElementById('dropdown_cover');
  if (drp.style.display === 'block') {
    cover.style.display = 'none'
    drp.style.display = 'none';
    tog.classList.remove('active');
    caret.classList.remove('active');
  } else {
    cover.style.display = 'block'
    drp.style.display = 'block';
    tog.classList.add('active');
    caret.classList.add('active');
  }
}
