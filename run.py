#-*- coding: utf-8 -*-
from flask import Flask, render_template, session, flash, redirect, url_for, request, jsonify, send_from_directory, Response, make_response
from config import *
import db
import redis 
from functools import wraps
from hashlib import sha256, md5
from collections import Counter
import random
from digest import digest
from send_mail import send_mail, enqueue_mail
import pytz
from datetime import datetime, timedelta
from error import UsageError
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException
import pandas as pd
import subprocess
import os

from flask_wtf import FlaskForm as Form, RecaptchaField
from wtforms import PasswordField, TextAreaField, StringField, RadioField, BooleanField
from wtforms.validators import DataRequired, URL, Regexp, Email
from wtforms.fields.html5 import EmailField

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['RECAPTCHA_PUBLIC_KEY'] = CAPTCHA_PUBLIC
app.config['RECAPTCHA_PRIVATE_KEY'] = CAPTCHA_PRIVATE
app.secret_key = SECRET_KEY
sr = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)

PDF_SCHEDULES_PATH = 'tmp'
LOGIN_ART = ['_3', '''Vue d&rsquo;un immeuble à São Paulo (Brésil), le 19 mars 2020. &copy; Victor <span class='pc'>Moriyama</span>/ <em>The New York Times</em>/ Redux/ Réa. <em>Emprunté à</em> <a href='https://www.philomag.com/articles/voisin-voisine' target='_blank' class='ext'>Philosophie magazine</a>.''']

class ContactGroup(Form):
  message = TextAreaField('message', validators=[DataRequired()])

  def validate(self):
    rv = Form.validate(self)
    if not rv:
      return False
    return True


class Login(Form):
  password = PasswordField('password', validators=[DataRequired()])
  email = EmailField('email', validators=[DataRequired(), Email()])

  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)

  def validate(self):
    rv = Form.validate(self)
    if not rv:
      return False

    email = self.email.data
    try:
      password = self.password.data.encode('utf-8')
      hp = sha256(password).hexdigest()
    except:
      self.password.errors.append('Your inputs could not be read.')
      return False

    if not db.check_admin_user(email, hp):
      self.password.errors.append('Invalid login.')
      return False
    else:
      session['user'] = email
      db.admin_update_login_date(email)
      return True


class ForgotPassword(Form):
  email = EmailField('email', validators=[DataRequired(), Email()])
  recaptcha = RecaptchaField('captcha')

  def validate(self):
    rv = Form.validate(self)
    if not rv:
      return False

    email = self.email.data
    if db.admin_email_exists(email):
      user = db.get_admin_user(email)
      email = user.get('email')
      temp_pw = random_pw(length=30)
      hashed_temp_pw = sha256(temp_pw.encode('utf8')).hexdigest()
      mailing = send_mail(email, 'forgotten password',
                          forgot_pw_mail_txt(temp_pw),
                          render_template('email.html', body=forgot_pw_mail_html(temp_pw)))
      if mailing:
        sr.set(email, hashed_temp_pw)
        sr.expire(email, 60 * 10) # 10 mins

    flash('The provide address has been sent recovery information if there exists an account registered against it. Please check your spam folder.')
    return True


class RecoverAccount(Form):
  email = EmailField('email', validators=[DataRequired(), Email()])
  code = StringField('authorisation code', validators=[DataRequired()])
  recaptcha = RecaptchaField('captcha')

  def validate(self):
    rv = Form.validate(self)
    if not rv:
      return False

    code = self.code.data
    hashed_code = sha256(self.code.data.encode('utf-8')).hexdigest()
    email = self.email.data
    
    hashed_ans = sr.get(email)
    if hashed_ans is None or hashed_code != hashed_ans.decode('utf-8'):
      self.code.errors.append('The authorisation code submitted expired, is incorrect or never existed.')
      return False
    else:
      pw = random_pw()
      hpw = sha256(pw.encode('utf8')).hexdigest()

      mailing = send_mail(email, 'new admin password',
                          new_user_mail_txt(pw),
                          render_template('email.html', body=new_user_mail_html(pw, email)))
      if mailing:
        db.admin_change_pw_insec(email, hpw)
        flash('<span class="no_hyphens">%s</span> has been sent a temporary password. Please check your spam folder.' % email)
        sr.delete(email)
        return True
      else:
        self.email.errors.append('We could’t send a new password to the provided address. Contact the development group if this persists.')
        return False


class ChangePassword(Form):
  curpassword = PasswordField('old password', validators=[DataRequired()])
  newpassword = PasswordField('new password', validators=[DataRequired()])
  newpassword2 = PasswordField('new password again', validators=[DataRequired()])

  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)

  def validate(self):
    rv = Form.validate(self)
    if not rv:
      return False

    cp = self.curpassword.data.encode('utf-8')
    hcp = sha256(cp).hexdigest()

    np = self.newpassword.data.encode('utf-8')
    hnp = sha256(np).hexdigest()

    np2 = self.newpassword2.data.encode('utf-8')

    email = session.get('user')
    if not db.check_admin_user(email, hcp):
      self.curpassword.errors.append('Invalid current password.')
      return False
    elif len(np) < 7:
      self.newpassword.errors.append('Your new password must be at least seven characters.')
      return False
    elif np != np2:
      self.newpassword.errors.append('Your new passwords don&rsquo;t match.')
      return False
    elif np == cp:
      self.newpassword.errors.append('Your new password can&rsquo;t be the same as your current password.')
      return False
    else:
      db.admin_change_pw(email, hcp, hnp)
      flash('Your password has been successfully changed.')
      return True


class NewAdmin(Form):
  password = PasswordField('password', validators=[DataRequired()])
  email = EmailField('email', validators=[DataRequired(), Email()])
  name = StringField('name', validators=[DataRequired()])
  level = BooleanField('super-admin')

  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)

  def validate(self):
    rv = Form.validate(self)
    if not rv:
      return False

    creator = session.get('user')
    if not creator:
      return False
    creator_pw = self.password.data.encode('utf-8')
    creator_hpw  = sha256(creator_pw).hexdigest()
    if not db.check_admin_user(creator, creator_hpw):
      self.password.errors.append('Invalid password.')
      return False

    email = self.email.data
    name = self.name.data
    if db.admin_email_exists(email):
      self.email.errors.append('An account is <a href=\'%s\'>already registered</a> against %s. Delete the account or tell the user to go through the password-reset process accessible from login.' % (url_for('.control_admins', h=email), email))
      return False
    else:
      if "'" in name or '"' in name:
        self.name.errors.append('The name field cannot contain inverted commas.')
        return False

      pw = random_pw()
      hpw = sha256(pw.encode('utf8')).hexdigest()

      mailing = send_mail(email, 'new account',
                          new_user_mail_txt(pw),
                          render_template('email.html', body=new_user_mail_html(pw, email)))
      if mailing:
        level = 2 if self.level.data else 1
        db.admin_add_user(email, hpw, creator, name, level=level)
        flash('A new account has been successfully registered against %s.' % email)
        return True
      else:
        self.email.errors.append('We could’t send an email to the provided address. Contact the development group if this persists.')
        return False

class DeleteAdmin(Form):
  password = PasswordField('password', validators=[DataRequired()])
  email = EmailField('email', validators=[DataRequired(), Email()])

  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)

  def validate(self):
    rv = Form.validate(self)
    if not rv:
      return False

    creator = session.get('user')
    if not creator:
      return False
    creator_pw = self.password.data.encode('utf-8')
    creator_hpw  = sha256(creator_pw).hexdigest()

    if not db.check_admin_user(creator, creator_hpw):
      self.password.errors.append('Invalid password.')
      return False


    email = self.email.data
    to_delete = db.get_admin_user(email)
    if to_delete is None:
      self.email.errors.append('No Admin user is registered against that email address.')
      return False
    elif to_delete['level'] > 1:
      self.email.errors.append('%s is a super-admin; their account can be deleted only via a request to the development group, sent from an address registered against a super-admin account.' % email)
      return False

    db.delete_admin_user(email)
    flash('Admin user %s has been deleted.' % email)
    return True



def random_pw(length=10):
  return ''.join(random.choice(PW_WELL) for x in range(length))


def forgot_pw_mail_html(pw):
  return '''
<div class='block_a_code'>
<p>A forgotten-password form was submitted with this address for %s. To proceed, click below within ten minutes of receipt of this message. No action is required if you didn&rsquo;t forget your password.</p>
<div class='a_code'><a href='%s?c=%s'>reset&nbsp;&rarr;</a></div>
</div>
  ''' % (APP_NAME, ADMIN_FORGOT_PW_URL, pw)

def forgot_pw_mail_txt(pw):
  return '''A forgotten-password form was submitted with this address for %s. To proceed, click: %s?c=%s. No action is required if you did not forget your password.
  ''' % (APP_NAME, ADMIN_FORGOT_PW_URL, pw)


def new_user_mail_html(pw, email, recovery=False):
  return '''
<div class='block_a_code'>
<p>A new account has been registered against this address for %s. Please use the following temporary password to <a href='%s/login?email=%s'>log in</a>:</p>
<div class='a_code'>%s</div>
</div>
  ''' % (APP_NAME, ADMIN_URL, email, pw)

def new_user_mail_txt(pw, recovery=False):
  return '''A new account has been registered against this address for %s. Log in at [%s] with the address receiving this message and the following temporary password: %s''' % (APP_NAME, ADMIN_URL, pw)

def loggedin():
  return session.get('user') is not None


@app.errorhandler(UsageError)
def handle_error(error):
  response = jsonify(error.to_dict())
  response.status_code = error.status_code
  return response


def require_login(view):
  @wraps(view)
  def protected_view(*args, **kwargs):
    if loggedin():
      user = db.get_admin_user(session.get('user'))
      if not user:
        # cookie exists but no user in db
        session.pop('user', None)
        flash('Your account was deleted while you were logged-in.')
        return redirect(url_for('.login'))
      elif user.get('must_change_pw') and (request.path not in ['/password','/logout']):
        flash('You must change your password to continue.')
        return redirect(url_for('.change_pw'))

      return view(*args, **kwargs)
    else:
      flash('Please log in to continue. You may have been logged-out due to a server update.')
      return redirect(url_for('.login', returnto=request.path))
  return protected_view

def require_login_api(view):
  @wraps(view)
  def protected_view(*args, **kwargs):
    if loggedin():
      user = db.get_admin_user(session.get('user'))
      if not user:
        # cookie exists but no user in db
        session.pop('user', None)
        raise UsageError('You were logged-out, or a technical update was just published; please refresh the page')
      return view(*args, **kwargs)
    else:
      raise UsageError('You were logged-out, or a technical update was just published; please refresh the page')
  return protected_view



@app.route('/favicon.ico')
def fav():
  return send_from_directory('static/img', 'pfd.ico')


@app.route('/login', methods=['GET', 'POST'])
def login():
  if loggedin():
    return redirect(url_for('.index'))
  form = Login()
  if form.validate_on_submit():
    if 'returnto' not in request.args:
      return redirect(url_for('.index'))
    else:
      return redirect(request.args.get('returnto'))

  return render_template('login.html',
                         title='login',
                         first_admin_email=FIRST_ADMIN_EMAIL,
                         first_admin_name=FIRST_ADMIN_NAME,
                         email=request.args.get('email'),
                         css=digest('css/styles.css'),
                         art=LOGIN_ART,
                         lab_name=LAB_NAME,
                         lab_full_name=LAB_FULL_NAME,
                         lab_site=LAB_SITE,
                         lab_location=LAB_LOCATION,
                         form=form)

@app.route('/logout')
@require_login
def logout():
  if session.get('new_emergency_perm_expiry'):
    session.pop('new_emergency_perm_expiry', None)

  session.pop('user', None)
  flash('Your session has been successfully closed.')
  return redirect(url_for('.login'))

@app.route('/forgot', methods=['GET', 'POST'])
def forgot_password():
  if loggedin():
    return redirect(url_for('.index'))

  form = ForgotPassword()
  if form.validate_on_submit():
    return redirect(url_for('.login'))
  return render_template('admin_forgot.html',
                         title='forgotten password',
                         css=digest('css/styles.css'),
                         lab_name=LAB_NAME,
                         lab_site=LAB_SITE,
                         form=form)


@app.route('/password', methods=['GET', 'POST'])
@require_login
def change_pw():
  user = db.get_admin_user(session.get('user'))

  form = ChangePassword()
  if form.validate_on_submit():
    return redirect(url_for('.index'))
  return render_template('change_password.html',
                         user=user,
                         title='change password',
                         css=digest('css/styles.css'),
                         lab_name=LAB_NAME,
                         lab_site=LAB_SITE,
                         form=form)


@app.route('/recover', methods=['GET', 'POST'])
def recover_password():
  cand = request.args.get('c')
  if cand is None:
    return redirect(url_for('.login'))

  form = RecoverAccount()
  if form.validate_on_submit():

    return redirect(url_for('.login'))

  return render_template('admin_recover.html',
                         title='recover account',
                         auth_code=cand,
                         lab_name=LAB_NAME,
                         lab_site=LAB_SITE,
                         css=digest('css/styles.css'),
                         form=form)


@app.route('/new', methods=['GET', 'POST'])
@require_login
def new_admin():
  user = db.get_admin_user(session.get('user'))
  if user['level'] < 2:
    return redirect(url_for('.index'))
  else:
    form = NewAdmin()
    if form.validate_on_submit():
      return redirect(url_for('.control_admins', h=form.email.data))
    return render_template('new_admin.html',
                           user=user,
                           title='new admin',
                           email=request.args.get('email'),
                           css=digest('css/styles.css'),
                           lab_name=LAB_NAME,
                           lab_site=LAB_SITE,
                           form=form)


@app.route('/admins', methods=['GET', 'POST'])
@require_login
def control_admins():
  user = db.get_admin_user(session.get('user'))
  if user['level'] < 2:
    return redirect(url_for('.index'))
  else:
    form = DeleteAdmin()
    if form.validate_on_submit():
      return redirect(url_for('.control_admins'))
    return render_template('control_admins.html',
                           title='control admins',
                           wide=True,
                           user=user,
                           highlight=request.args.get('h'),
                           css=digest('css/styles.css'),
                           admin_users=db.get_admin_users(),
                           lab_name=LAB_NAME,
                           lab_site=LAB_SITE,
                           form=form)


@app.route('/')
@require_login
def index():
  user = db.get_admin_user(session.get('user'))
  resp = make_response(render_template('index.html',
                                       user=user,
                                       title='list people',
                                       angular='pfhApp',
                                       base='/',
                                       cc=COUNTRY_CODE,
                                       lab_name=LAB_NAME,
                                       js=[digest('js/appointment_chart.js'),
                                           digest('js/controllers.js')],
                                       css=digest('css/styles.css')))
  resp.set_cookie('rgpd', '1')
  return resp


@app.route('/table')
@require_login
def table():
  return render_template('tables.html', user=user,
                         css=digest('css/styles.css'),
                         title='table')


def verify_phone(number, name):
  tw_client = Client(TWILIO_SID, TWILIO_AUTH)
  try:
    number_resp = tw_client.lookups.phone_numbers(number).fetch(country_code=COUNTRY_CODE)
  except TwilioRestException:
    raise UsageError('That phone number doesn’t appear to be valid')
  if number_resp is not None or number_resp.national_format is None:
    person = db.get_person(name, number_resp.phone_number)
    if person.count() > 0:
      raise UsageError('%s is already registered against %s.' % (number_resp.phone_number, name))
    else:
      return number_resp
  else:
    raise UsageError('That phone number could not be verified')

@app.route('/add_person', methods=['POST'])
@require_login_api
def add_person():
  user = db.get_admin_user(session.get('user'))
  if request.json is None:
    raise UsageError('no POST data')

  number = request.json.get('phone')
  if number is None or type(number) != str:
    raise UsageError('no number provided')
  number = number.replace(' ', '').replace('(', '').replace(')', '').replace('-', '')

  name = request.json.get('name')
  if name is None or type(name) != str:
    raise UsageError('no name provided')


  walk_in = request.json.get('walk_in') == True

  number_resp = verify_phone(number, name)
  db.add_person(name, number_resp.phone_number,
                number_resp.national_format, walk_in, user.get('email'))
  return jsonify({
    'ok': 1,
    'highlight': { 'name': name, 'phone': number_resp.phone_number } if walk_in else {},
    'message': '%s, %s has been added' % (name, number_resp.national_format)
  })
  

def naive_dt_to_local_str(dt, fmt='%a., %-d %b %Y, %H:%M', otz='UTC'):
  tz = pytz.timezone(TZ_KEY)
  ndt = dt.replace(tzinfo=pytz.timezone(otz))
  return ndt.astimezone(tz).strftime(fmt)


def pop_to_dt_str(people):
  for p in people:
    for k in ['ts', 'waiting_since', 'scheduled_since',
              'first_dose_receipt_recorded', 'second_dose_receipt_recorded']:
      v = p.get(k)
      if v:
        p[k] = naive_dt_to_local_str(v)

    for k in ['first_dose_dt', 'second_dose_dt']:
      v = p.get(k)
      if v:
        p[k] = naive_dt_to_local_str(v, otz=TZ_KEY)

    if p.get('dob'):
      p['dob'] = p['dob'].strftime('%-d %b %Y')

  return people


def get_pop_with_status(status):
  q = request.args.get('q')
  filter_date = request.args.get('date')
  if filter_date is not None:
    try:
      filter_date = datetime.strptime(filter_date, '%Y-%m-%d')
    except ValueError:
      raise UsageError('The date was improperly formatted')

  if q and len(q) > 2:
    if filter_date:
      ret = list(db.get_people_with_status_query_and_date(status, q, filter_date))
    else:
      return list(db.get_people_with_status_and_query(status, q))
  elif filter_date:
    ret = list(db.get_people_with_status_and_date(status, filter_date))
  else:
    return list(db.get_people_with_status(status))


  # filter_date has been provided. Check which dose falls on filter_date
  for p in ret:
    first_dose = p.get('first_dose_dt')
    p['filter_date_first_dose_delta'] = (first_dose - filter_date).days if first_dose is not None else -1

  return ret



def paginate(li, size=12):
  lli = len(li)
  return lli, [li[i: i + size] for i in range(0, lli, size)]


@app.route('/get_to_contact')
@require_login_api
def get_to_contact():
  pop = pop_to_dt_str(sorted(get_pop_with_status(STATUS_TO_CONTACT), key=lambda x: x.get('ts')))
  npeople, people = paginate(pop)
  return jsonify({
    'people': people,
    'npeople': npeople,
    'ok': 1
  })


def get_name_and_phone():
  phone = request.json.get('phone')
  if phone is None or type(phone) != str:
    raise UsageError('No phone number provided')

  name = request.json.get('name')
  if name is None or type(phone) != str:
    raise UsageError('No name provided')

  if db.get_person(name, phone).count() == 0:
    raise UsageError('No person by that phone number is recorded. Someone may have already deleted them; refresh the page')
  return name, phone

@app.route('/delete', methods=['POST'])
@require_login_api
def delete():
  if request.json is None:
    raise UsageError('No POST data')

  name, phone = get_name_and_phone()

  db.rm_person(name, phone)
  return jsonify({
    'ok': 1,
    'message': 'Person removed'
  })




@app.route('/save_contact', methods=['POST'])
@require_login_api
def save_to_contact():
  if request.json is None:
    raise UsageError('No POST data')


  name, phone = get_name_and_phone()
  person = db.get_person(name, phone)[0]

  elig = request.json.get('justification')
  if elig is None or type(elig) != str:
    elig = ''

  skin_colour, is_hispanic = None, None
  if COUNTRY_CODE == 'US':
    # 'race' and 'ethnicity' are required in the USA
    skin_colour = request.json.get('skin_colour')
    if skin_colour is None or type(skin_colour) != str:
      raise UsageError('No skin colour provided')

    is_hispanic = request.json.get('is_hispanic') == True

  dob = request.json.get('nice_dob')
  if dob is not None:
    tz = pytz.timezone(TZ_KEY)
    try:
      dob = tz.localize(datetime.strptime(dob, '%Y-%m-%d'))
    except ValueError:
      raise UsageError('The date of birth was improperly formatted')

  db.set_elig_and_dob(name, phone, elig, dob, skin_colour,
                      is_hispanic, request.json.get('patient') == True)
  return jsonify({
    'ok': 1,
    'highlight': { 'name': name, 'phone': phone } if person.get('walk_in') else {},
    'message': 'Saved'
  })
  

@app.route('/get_waiting')
@require_login_api
def get_waiting():
  pop = pop_to_dt_str(sorted(get_pop_with_status(STATUS_CONTACTED),
    key=lambda x: x.get('waiting_since')))
  npeople, people = paginate(pop)
  return jsonify({
    'people': people,
    'npeople': npeople,
    'ok': 1
  })


def get_all_times_for_day(d):
  weekday = d.weekday()
  times = []
  if weekday == 5:
    for i in range(8, 17):
      times += ['%s%s:00' % ('0' if i < 10 else '', i), '%s%s:30' % ('0' if i < 10 else '', i)]
    times.pop(-1)
    times.pop(-1)
  elif weekday != 6:
    for i in range(9, 19):
      times += ['%s%s:00' % ('0' if i < 10 else '', i), '%s%s:30' % ('0' if i < 10 else '', i)]
    times.pop(-1)
  if d.date() == datetime.now().date():
    return filter_today_times(times)
  else:
    return times


def filter_today_times(times):
  tz = pytz.timezone(TZ_KEY)
  now = datetime.now(tz)
  res = []
  for t in times:
    full_str = '%s %s' % (now.strftime('%Y-%m-%d'), t)
    dt = datetime.strptime(full_str, '%Y-%m-%d %H:%M').replace(tzinfo=tz)
    if dt > now:
      res.append(t)

  return res

def doses_per_block(pday):
  weekday = pday.weekday()
  if weekday == 5:
    # Saturday
    return 20
  elif weekday != 6:
    # not Sunday
    return 20
  else:
    # Sunday
    return 0

def available_times(pday):
  all_times = get_all_times_for_day(pday)
  first_dose_times = list(map(lambda x: x.get('first_dose_time'), list(db.get_schedule_for_day(pday))))
  seuil = doses_per_block(pday)
  counts = Counter(first_dose_times)
  taken_times = list(filter(lambda x: counts[x] >= seuil, counts))

  at = set(all_times) - set(taken_times)
  return sorted(list(at), key=lambda x: int(x.replace(':','')))

  
@app.route('/open_dates', methods=['POST'])
@require_login_api
def open_dates():
  pday = request.json.get('nice_possible_day')
  if pday is None:
    raise UsageError('No query provided')
  
  tz = pytz.timezone(TZ_KEY)
  try:
    pday = tz.localize(datetime.strptime(pday, '%Y-%m-%d'))
  except ValueError:
    raise UsageError('The day was improperly formatted')

  # Must add am/pm for Americans
  available = available_times(pday)
  for i in range(len(available)):
    a = available[i]
    available[i] = [a, datetime.strptime(a, '%H:%M').strftime('%I:%M %p')]

  return jsonify({
    'ok': 1,
    'times': available
  })


@app.route('/save_time', methods=['POST'])
@require_login_api
def save_time():
  day = request.json.get('nice_possible_day')
  time = request.json.get('selected_time')
  if not all([day, time]):
    raise UsageError('No datetime or no phone number provided')

  try:
    day = datetime.strptime(day, '%Y-%m-%d')
  except ValueError:
    raise UsageError('The day was improperly formatted')

  name, phone = get_name_and_phone()
  person = db.get_person(name, phone)[0]


  tz = pytz.timezone(TZ_KEY)
  full_str = '%s %s' % (day.strftime('%Y-%m-%d'), time)
  try:
    appt_dt = datetime.strptime(full_str, '%Y-%m-%d %H:%M')
  except ValueError:
    raise UsageError('The time was improperly formatted')

  available = available_times(day)
  if not time in available:
    raise UsageError('That time is not available; request another')

  scheduling_first_dose = person.get('first_dose_receipt_recorded') is None
  if scheduling_first_dose:
    db.set_first_dose_date(name, phone, appt_dt, time)
    override_walk_in = request.json.get('override_walk_in') == True
    if person.get('walk_in') and not override_walk_in:
      record_dose(name=name, phone=phone)
      return jsonify({
        'ok': 1,
        'message': 'Scheduled and dose recorded'
      })
  else:
    if person.get('first_dose_dt') is None:
      raise UsageError('You’re scheduling a second dose but no date of first dose has been recorded for this patient; no changes will be made to the record until you contact the development group')
    db.set_second_dose_date(name, phone, appt_dt, time)


  return jsonify({
    'ok': 1,
    'message': 'Scheduled'
  })


def get_scheduled_list():
  pop = get_pop_with_status(STATUS_SCHEDULED) # scheduled for first dose
  pop += get_pop_with_status(STATUS_HAS_FIRST_DOSE) # scheduled for second dose
  return sorted(pop, key=lambda x: x['first_dose_dt'] if not x.get('second_dose_dt') else x['second_dose_dt'])

def get_latest_appointment_date(p):
  if p['status'] == STATUS_HAS_FIRST_DOSE:
    first_date = p['second_dose_dt']
  else:
    first_date = p['first_dose_dt']
  return first_date


@app.route('/get_scheduled')
@require_login_api
def get_scheduled(): 
  s = get_scheduled_list()
  
  first_date, last_date = None, None
  if len(s) > 0:
    first_date = (get_latest_appointment_date(s[0]) - timedelta(days=1)).strftime('%Y-%m-%d')
    last_date = (get_latest_appointment_date(s[-1]) + timedelta(days=1)).strftime('%Y-%m-%d')

  graph_data = None
  if len(s) > 0 and not request.args.get('date') and not request.args.get('q'):
    n_appoint_day = {}
    for i in s:
      if i['status'] == STATUS_HAS_FIRST_DOSE:
        key = i['second_dose_dt']
      else:
        key = i['first_dose_dt']
      key = key.strftime('%Y-%m-%d')

      if n_appoint_day.get(key):
        n_appoint_day[key] += 1
      else:
        n_appoint_day[key] = 1

    graph_data = 'date, appointments\n'
    for d in n_appoint_day:
      graph_data += ('%s, %s\n' % (d, n_appoint_day[d]))


  npeople, people = paginate(pop_to_dt_str(s))
  return jsonify({
    'people': people,
    'npeople': npeople,
    'first_date': first_date,
    'last_date': last_date,
    'graph_data': graph_data,
    'ok': 1
  })


@app.route('/unschedule', methods=['POST'])
@require_login_api
def unschedule():
  name, phone = get_name_and_phone()
  person = db.get_person(name, phone)[0]

  if person.get('first_dose_receipt_recorded') is not None:
    # received first dose, rescheduling second
    db.move_back_to_waitlist(name, phone, dose='second')
  else:
    db.move_back_to_waitlist(name, phone, dose='first')

  return jsonify({
    'ok': 1,
    'highlight': { 'name': name, 'phone': phone }
  })


@app.route('/record_dose', methods=['POST'])
@require_login_api
def record_dose(name=None, phone=None):
  if not name or not phone:
    name, phone = get_name_and_phone()

  person = db.get_person(name, phone)[0]

  status = person.get('status')
  if status == STATUS_SCHEDULED:
    # recording receipt of first dose
    first_dose_dt = person.get('first_dose_dt')
    first_dose_time = person.get('first_dose_time')
    if not all([first_dose_dt, first_dose_time]):
      raise UsageError('No first dose appointment; contact the development group')

    sd = request.json.get('single_dose')
    single_dose = sd is not None and sd == True
    if single_dose:
      second_dose_dt = first_dose_dt
    else:
      second_dose_dt = first_dose_dt + timedelta(days=28)

    db.set_received_first_dose(name, phone, second_dose_dt,
                               first_dose_time, session.get('user'), single_dose=single_dose)

    if single_dose:
      db.set_received_second_dose(name, phone, single_dose, session.get('user'))

    return jsonify({
      'ok': 1,
      'message': 'first dose recorded'
    })
  elif status == STATUS_HAS_FIRST_DOSE:
    # recording receipt of second dose
    db.set_received_second_dose(name, phone, False, session.get('user'))
    return jsonify({
      'ok': 1,
      'message': 'second dose recorded'
    })

  raise UsageError('not scheduled for an appointment')


@app.route('/get_vaccinated')
@require_login_api
def get_vaccinated():
  pop = pop_to_dt_str(sorted(get_pop_with_status(STATUS_HAS_SECOND_DOSE),
    key=lambda x: x.get('second_dose_dt')))
  return jsonify({
    'ok': 1,
    'people': pop if request.args.get('q') else [],
    'npeople': len(pop) 
  })


@app.route('/change_info', methods=['POST'])
@require_login_api
def change_info():
  name, phone = get_name_and_phone()
  person = db.get_person(name, phone)[0]

  new_name = request.json.get('new_name')
  new_phone = request.json.get('new_phone')

  if new_name is None or new_phone is None:
    raise UsageError('No new name or no phone provided')

  if new_phone != person.get('phone_national'):
    number_resp = verify_phone(new_phone, new_name)
    db.change_info(name, phone, new_name, number_resp.phone_number, number_resp.national_format)
    person = db.get_person(new_name, number_resp.phone_number)
  else:
    db.change_info(name, phone, new_name, phone, person.get('phone_national'))
    person = db.get_person(new_name, phone)

  return jsonify({
    'ok': 1,
    'person': pop_to_dt_str(list(person))[0]
  })





@app.route('/unset_dose', methods=['POST'])
@require_login_api
def unset_dose():
  name, phone = get_name_and_phone()
  person = db.get_person(name, phone)[0]

  if not person.get('second_dose_receipt_recorded'):
    raise UsageError('receipt of second dose not recorded')


  if person.get('single_dose'):
    # one-dose; unschedule and unset receipt
    db.move_back_to_waitlist(name, phone, dose='second')
    db.move_back_to_waitlist(name, phone, dose='first')
  else:
    # two-dose; keep scheduled for second dose, just unset
    # record of its receipt
    db.unset_received_second_dose(name, phone)


  return jsonify({
    'ok': 1,
    'message': 'unset at least one dose'
  })


@app.route('/pdf')
@require_login
def pdf_schedule():
  date = request.args.get('date')
  is_patients = request.args.get('patients') and request.args.get('patients') == '1'
  if date is None and not is_patients:
    return redirect(url_for('index'))

  tz = pytz.timezone(TZ_KEY)
  now = datetime.now(tz)

  if date is not None:
    try:
      date = datetime.strptime(date, '%Y-%m-%d')
    except ValueError:
      raise UsageError('The date was improperly formatted')

    
    title = 'Schedule for %s' % date.strftime('%A %d %B %Y')
    people = get_scheduled_list()
  else:
    title = '%s patients in system' % (LAB_NAME)
    people = list(db.get_patients())

  npeople, people = paginate(people, size=30)

  if not is_patients:
    for page in people:
      prev_time = None
      for p in page:
        if p.get('first_dose_receipt_recorded') is not None:
          this_time = p.get('second_dose_time')
        else:
          this_time = p.get('first_dose_time')
        if not prev_time:
          prev_time = this_time
        elif prev_time != this_time:
          p['needs_sep_above'] = True
          prev_time = this_time

  user = db.get_admin_user(session.get('user'))
  latex = render_template('latex/schedule.tex',
                          title=title,
                          cc=COUNTRY_CODE,
                          is_patients=is_patients,
                          statuses=['to contact', 'waiting', 'scheduled',
                                    '1 dose of 2', 'vaccinated'],
                          status_colours=['c10000', 'c10000', 'c10000', '938b00', '3dbd03'],
                          generated=now.strftime('%d %B %Y %H:%M'),
                          author=user.get('email'),
                          dir=os.path.dirname(os.path.realpath(__file__)),
                          people=people,
                          npeople=npeople)

  ts = int(now.timestamp())
  fname = '%s.tex' % ts
  with open('%s/%s' % (PDF_SCHEDULES_PATH, fname),'w') as f:
    f.write(latex)

  cmd = ['xelatex', '-interaction', 'nonstopmode', fname]
  proc = subprocess.Popen(cmd, cwd=PDF_SCHEDULES_PATH)
  proc.communicate()

  ret = proc.returncode
  if ret != 0:
    os.unlink('%s/%s.pdf' % (PDF_SCHEDULES_PATH, ts))
    raise UsageError('could not generate pdf; contact development group')


  os.unlink('%s/%s' % (PDF_SCHEDULES_PATH, fname))
  os.unlink('%s/%s.log' % (PDF_SCHEDULES_PATH, ts))
  return redirect(url_for('tmp_schedule', filename=('%s.pdf' % ts)))


@app.route('/tmp/<filename>')
@require_login
def tmp_schedule(filename):
  return send_from_directory(PDF_SCHEDULES_PATH, filename)




@app.route('/support', methods=['GET', 'POST'])
@app.route('/assistance', methods=['GET', 'POST'])
@require_login
def contact_group():
  user = db.get_admin_user(session.get('user'))
  form = ContactGroup()
  if form.validate_on_submit():
    txt = 'Message from %s (%s): %s' % (user.get('email'), user.get('name'), form.message.data)

    tz = pytz.timezone(TZ_KEY)
    nowstr = datetime.now(tz).strftime('%A %d %B %Y at %H:%M')
    enqueue_mail(GROUP_EMAIL, 'assistance message',
                 txt, render_template('email.html', body=('<p>%s</p><p style="color:grey">(It&rsquo;s %s in %s.)</p>' % (txt, nowstr, TZ_KEY))))
    flash('Thanks; we’ll be in touch soon.')
    return redirect(url_for('.index'))

  return render_template('assistance.html',
                         user=user,
                         form=form,
                         lab_name=LAB_NAME,
                         lab_site=LAB_SITE,
                         title='contact the development group',
                         css=digest('css/styles.css'))

@app.route('/export')
@require_login
def export():
  user = db.get_admin_user(session.get('user'))
  return render_template('export.html',
                         user=user,
                         lab_name=LAB_NAME,
                         lab_site=LAB_SITE,
                         title='export data',
                         css=digest('css/styles.css'))


@app.route('/people.csv')
@require_login
def export_all():
  people = db.get_people()
  df = pd.DataFrame.from_dict(people)
  csv = df.to_csv(index=False)
  return Response(csv,
                  mimetype='text/csv',
                  status=201)





if __name__ == '__main__':
  app.run(debug=DEBUG, port=4555, host='0.0.0.0')
