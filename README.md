# Foreword Covid-19 vaccination scheduler

This system allows you to schedule appointments for Covid-19 vaccinations. It is intended for the staff of laboratories that perform vaccinations; it does not have a public-facing (patient) use. You must clone the repository, create `config.py` (from `config.py.default`) and `templates/phase.html` (detailing populations eligible in your country, from `templates/phase.html.default`), edit `templates/support.html` and put it on your own server. It is developed by [Foreword Development](https://foreword.dev), to which you may pay a nominal fee to host the system for you: [info@foreword.dev](mailto:info@foreword.dev).

Due to time constraints, it is currently available only in (British) English, but we welcome localisation commits. Candidates for vaccination are identified by phone number and names. Phone numbers of most countries are supported.

*N.b.* : commercial use is prohibited; please see [LICENCE.rst](LICENCE.rst) for more information.

<img src="static/img/screenshot.png" width="500px" alt='Foreword Covid-19 vaccination scheduler'>

